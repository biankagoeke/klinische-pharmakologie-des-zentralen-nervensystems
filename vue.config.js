module.exports = {
    publicPath: process.env.PUBLIC_PATH || '/',
    pages: {
        index: {
            entry: 'src/main.ts',
            title: 'Klinische Pharmakologie des Zentralen Nervensystems',
        },
    },
    chainWebpack: config => {
        config.module
            .rule('markdown')
            .test(/\.md$/)
            .use('frontmatter-markdown-loader')
            .loader('frontmatter-markdown-loader')
            .tap(() => {
                const markdownIt = require('markdown-it')({
                    html: true
                });

                markdownIt.renderer.rules.table_open = function () {
                    return '<table class="table table-striped">\n';
                };

                const image = markdownIt.renderer.rules.image;
                markdownIt.renderer.rules.image = function(tokens, idx, options, env, slf) {
                    const token = tokens[idx];
                    token.attrPush(['class', 'img-fluid']);
                    return image(tokens, idx, options, env, slf);
                };

                return { markdownIt };
            })
            .end()
    }
};
