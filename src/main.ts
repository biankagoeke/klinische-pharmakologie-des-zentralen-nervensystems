import { createApp } from 'vue'
import App from './App.vue'
import './main.scss';
import {createRouter, createWebHashHistory} from "vue-router";
import {createChapters} from "@/contents";
import StartView from "@/components/StartView.vue";
import ChapterView from "@/components/ChapterView.vue";
import QuizView from "@/components/QuizView.vue";

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        { path: '/', name: 'start', component: StartView },
        { path: '/:id', name: 'chapter', component: ChapterView, props: true },
        { path: '/quiz', name: 'quiz', component: QuizView }
    ]
});

createApp(App)
    .use(createChapters())
    .use(router)
    .mount('#app')
