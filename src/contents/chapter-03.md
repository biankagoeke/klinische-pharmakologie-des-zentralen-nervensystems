---
id: parkinson
title: Praxisbeispiel Parkinson
---

### Pharmazeutische Wirkung Parkinson

Wir wechseln nun die Thematik und behandeln eine Nervenkrankheit, die ebenfalls Schmerzen
auslöst und an der in Deutschland mehr als 250.000 Menschen leiden, der Parkinson-Krankheit bzw.
Morbus Parkinson. Diese Erkrankung wird umgangssprachlich auch als „Schüttellähmung“ bezeichnet
und wurde nach dem englischen Arzt Dr. James Parkinson benannt, der diese Art der
Bewegungsstörung 1817 erstmals beschrieben hat.

Zum Verständnis der Krankheit klären wir zunächst was diese Krankheit ist und wie sie sich
auf den Menschlichen Körper auswirkt. Parkinson ist gekennzeichnet durch das vornehmliche
Absterben der dopaminproduzierenden Nervenzellen in der Substantia nigra, einer Struktur
im Mittelhirn. Ein Mangel des Botenstoffs Dopamin führt letztlich zu einer Verminderung der
aktivierenden Wirkung der Basalganglien, Kerngebieten des Endhirns, auf die Großhirnrinde
und somit zu Bewegungsstörungen.

Die klassischen vier Hauptsymptome der Parkinson-Krankheit sind: Bradykinese, Rigor,
Tremor und posturale Instabilität.

Bei einer Bradykinese handelt es um eine Verlangsamung der Willkürmotorik. Dies bedeutet,
dass Betroffene sich langsamer und weniger bewegen. Aufstehen, Gehen, Drehen bereitet
Schwierigkeiten. Bewegungen zu initiieren, ist manchmal kaum möglich, es entsteht eine
Bewegungsblockade (Akinese).

Als Rigor bezeichnet man eine gesteigerte Grundspannung der Skelettmuskulatur, die sich bei
der passiven Bewegung einer Extremität als konstanter Widerstand bemerkbar macht.
Oftmals sind zunächst die Nacken- und Schultermuskeln betroffen, häufig einseitig.

Ein Ruhetremor ist eine Zitterbewegung (Tremor), die in körperlicher Ruhe auftritt und bei
gezielten Bewegungen oft geringer wird. Am häufigsten sind die Hände und Füße betroffen,
in schweren Fällen allerdings noch weitere Gliedmaßen.

Posturale Instabilität ist eine Störung der aufrechten Körperhaltung, die durch mangelhafte
Halte- und Stellreflexe ausgelöst wird. Diese Störung wirkt sich auf die Balance der
Betroffenen aus. Plötzliche Bewegungen, zum Beispiel durch einen unerwarteten Stoß,
können nicht mehr aufgefangen werden. Der Gang wird insgesamt unsicher, die Betroffenen
neigen zu Stürzen.

Neben der deutlichen Einschränkung der Bewegung leiden Parkinson-Patienten ebenso an
Schmerzen. Im frühen Stadium der Krankheit treten oftmals Schmerzen im Bereich der Schultern, im
Nacken sowie den Armen auf. Zusätzlich können auch die Beine sowie die gesamte
Rückenmuskulatur betroffen sein, welche durch krankheitsbedingte Muskelsteifheit (Rigor)
versursacht wird. Zusätzlich leiden Patienten auch an so genannten muskuloskeletalen Schmerzen
und es kommt oftmals zu einseitig betonten krampfartigen Schmerzen in Waden, Füßen und Zehen.

Diese treten meist in den frühen Morgenstunden auf, wenn die Wirkung der Medikamente
abgeklungen ist. Eine Schmerzlinderung kann je nach Ursprung der Schmerzen durch Parkinson
Medikamente oder eine zusätzliche Schmerztherapie erfolgen.

### Grundlagen Parkinson

Erläutern wir zuerst die Grundlagen der Parkinson-Krankheit. Wie bereits geschildert basiert die
Krankheit auf dem Untergang dopaminerger Neurone in der Substantia nigra und in den
Basalganglien mit konsekutivem Dopamin-Mangel. Bei gesunden Menschen stimuliert Dopamin
inhibitorische D2-Rezeptoren im Striatum, welche die striatale Blockade, also die Ausschüttung des
Neurotransmitters Gamma-Amino-Buttersäure GABA, mit Auswirkung auf das zentrale Nervensystem
(ZNS), hemmen. Somit findet eine Enthemmung des motorischen Kortex statt und Entwürfe von
Bewegungsabläufen können leichter initiiert werden. Kommt es nun zu einem Verlust von Dopamin
im Striatum werden kortikale Bewegungsentwürfe durch den ungehinderten Einfluss von GABA
gehemmt, welches zu der typischen verminderten Beweglichkeit beim Morbus Parkinson führt.

Die pathogenetischen Vorgänge sind chronisch fortschreitend und sind bei Krankheitsmanifestation
schon weit fortgeschritten. Motorische Symptome treten auf, wenn bereits 30–70 % der
dopaminergen Neuronen geschädigt wurden. Durch Schädigung cholinerger, noradrenerger und
serotonerger Systeme und zum Teil auch des peripheren Nervensystems werden nicht-motorische
Symptome wie Depressionen und kognitive Einschränkungen, Störungen der Impulskontrolle sowie
des autonomen Nervensystems ausgelöst.

### Gruppen Parkinson

Menschen mit dieser Krankheit werden unterschiedlichen Gruppen zugeordnet, welche vom Alter,
der Verlaufsform und der Krankheitsphase abhängen.

**Einteilung nach Erkrankungsalter**

Der Morbus Parkinson kann anhand des Erkrankungsalters unterteilt werden in:

- juvenil: Manifestationalter < 21 Jahre
- „early onset“: Manifestationsalter 21–50 Jahre
- „late onset“: Manifestationsalter > 60 Jahre.

**Einteilung nach Verlaufsform**

- akinetisch-rigider Typ
- Äquivalenz-Typ
- Tremordominanz-Typ
- selten: monosymptomatischer Ruhetremor.

**Einteilung nach Krankheitsphase**

- Prodromalphase
- frühe Krankheitsphase: Jahre 1–5 der Erkrankung
- fortgeschrittene Krankheitsphase: ab dem 5. Jahr der Erkrankung, es treten Wirkungsfluktuationen auf
- späte Krankheitsphase: Demenz, Stürze, Halluzinationen.

Zur Behandlung von Parkinson gibt es diverse Überlegungen und Strategien. Da Anti-Parkinson-Mittel
allerdings nur gegen die motorischen Störungen helfen und nicht gegen die begleitenden klinischen
Symptome wie Depression, vegetative Störungen, erhöhte Schmerzempfindlichkeit und Demenz
müssen Patienten zusätzlich mit spezifischen Wirkstoffen therapiert werden. Pharmakologisch sind
verschiedene Therapiestrategien zur Kompensation des Dopaminverlusts denkbar, die in der
nachfolgenden Tabelle aufgeführt sind.

| Allgemeine Überlegung | Konkrete Therapiestrategie |
|---|---|
| kausale Therapie des Untergangs der Neurone | Neuroprotektion (bisher noch Bestandteil der Forschung) |
| Wiederherstellung der endogenen Dopamin Bildung | Zelltransplantation (bisher noch Bestandteil der Forschung) |
| Ersatz des fehlenden Transmitters | Gabe von L-Dopa in Kombination mit einem Decarbocylasehemmer |
| Erhalt der endogenen Restfunktion bzw. Hemmung des Abbaus von Dopamin | COMT- und MAO-B-Hemmstoffe |
| Agonisten am D2-Rezeptor | D2-Agonisten |
| Hemmung der Gegenspieler | Blockade des mACH-Rezeptors, des NMDARezeptors oder des Adenosin-2-Rezeptors |

*nach Herdegen, Kurzlehrbuch Pharmakologie und Toxikologie, Thieme, 2019*

### Therapieansatz

![](pharma_008100_angriffspunkte.jpg)

Der Therapieansatz beim Morbus Parkinson besteht
darin, das Absterben der dopaminergen Neurone
aufzuhalten oder zumindest die Fortschreitung zu
verzögern. Dies kann die Pharmakotherapie
allerdings noch nicht leisten.

Erläutern wir nun die pharmakologischen
Angriffspunkte der Anti-Parkinson-Mittel.

Der Abbau von L-3,4-Dihydroxyphenylalanin
(L-Dopa) erfolgt durch die Catechol-OMethyltransferase (COMT), der von
Dopamin zusätzlich durch die
Monoaminooxidase B (MAO-B). MAO-Bund COMT-Hemmer verzögern damit den
Dopaminabbau. Die nicht ZNS-gängigen
Hemmstoffe der Dopa-Decarboxylase (DDC)
vermindern die Umwandlung von L-Dopa in
Dopamin in der Peripherie. L-Dopa wird
daher immer mit Decarboxylasehemmern
kombiniert, damit ausreichend hohe Dosen
L-Dopa im Gehirn ankommen und das LDopa nicht schon in der Peripherie
umgesetzt wird.

Zur Behandlung von Parkinson werden L-Dopa, Dopaminrezeptor-Agonisten, NMDA-RezeptorAntagonisten, Selektive MAO-B-Hemmer, COMT-Hemmer sowie Anticholinergika verwendet, auf
welche wir im Nachfolgenden nun eingehen werden.

### Therapieansatz L-Dopa

Gehen wir zunächst auf das L-Dopa ein.

Bei der Behandlung werden hauptsächlich zwei unterschiedliche Präparate verwendet:

- L-Dopa in Kombination mit Carbidopa (Dopadura®, LevoCarb®)
- L-Dopa in Kombination mit Benserazid (Madopar®, Restex®).

Bei L-Dopa handelt es sich um eine Vorstufe von Dopamin, welches allerdings im Gegensatz
zu Dopamin ZNS-gängig ist. Dies bedeutet, dass L-Dopa die Blut-Hirn-Schranke des zentralen
Nervensystems durchdringen kann. L-Dopa wird im präsynaptischen Neuron durch die
Dopamin-Decarboxylase zum wirksamen Dopamin decarboxyliert und in synaptischen
Vesikeln gespeichert. Sein Einsatz setzt also noch funktionsfähige dopaminerge Neurone mit
einer Restfunktion der Dopamin-Decarboxylase voraus. Wird Dopamin nicht in die
synaptischen Vesikel aufgenommen, kann es im Zytoplasma zum radikalähnlichen DopaminChinon oxidiert werden.

Die Umwandlung in Dopamin beginnt allerdings schon im Gastrointestinaltrakt statt, dem
Hauptteil des Verdauungsapparates. Dies hat klinisch relevante Übelkeit zur Folge. Aus
diesem Grund wird L-Dopa immer mit einem Decarboxylasehemmer wie Benserazid,
Carbidopa kombiniert. Ohne diese Hemmer würden bereits 97–99 % des L-Dopas zu früh
umgewandelt werden. Die Decarboxylasehemmer binden sich an das katalytische Zentrum
der Dopamin-Decarboxylase und blockieren sie somit, wodurch der Abbau von L-Dopa
verhindert wird. So wird sichergestellt, dass genug L-Dopa ins Gehirn gelangt, wo es zum
wirksamen Dopamin umgewandelt wird.

L-Dopa wird den Patienten stets oral verabreicht, wobei es am schnellsten in Form von
Brause Tabletten wirkt. Um eine adäquate Wirkung zu erhalten, muss L-Dopa über mehrere
Wochen langsam aufdosiert werden.

Im Laufe der Behandlung mit L-Dopa zeigen sich vermehrt Wirkungsfluktuationen, die
Anpassungen wie die Verkürzung der Einnahmeintervalle notwendig machen. Den genauen
Wirkungsverlauf von L-Dopa erklären wir anhand dieser Grafik.

![](pharma_008100_l-dopa.jpg)

Zu Beginn der Therapie ist das therapeutische Fenster von L-Dopa noch breit. Mit
zunehmender Dauer der Einnahme verschmälert sich das therapeutische Fenster und die
notwendigen Dosierungen werden höher. Zudem nimmt die Wirkdauer ab und es entwickeln
sich häufiger Dyskinesien.

L-Dopa wird besonders bei Patienten mit Symptomen von Akinese und Rigor eingesetzt, um
diese zu mindern. Aufgrund des früher oder später eintretenden Wirkungsverlusts wird die
Therapie bei jüngeren Patienten oder nur leichter Symptomatik zunächst meist mit einem
Dopamin-Rezeptor-Agonisten begonnen.

So wie bei fast allen Medikamenten treten auch bei der Behandlung mit L-Dopa unerwünschte
Wirkungen auf, welche wir im Folgenden erläutern.

Zu den Nebenwirkungen zählen Schwitzen, Übelkeit, Erbrechen, verminderte Verdauung
sowie Verstopfungen. Zudem kann es zu Kreislaufbeschwerden bei plötzlichen Bewegungen,
wie z.B. dem Aufstehen kommen welche von Herzrasen begleitet sind.

Psychische Nebenwirkungen beinhalten Unruhe, Verwirrtheit bis hin zu Halluzinationen,
psychotisches Erleben und Depressionen. Die motorischen Nebenwirkungen nehmen im
Verlauf der Behandlung zu. Die unerwünschten Wirkungen nehmen mit verlängerter
Behandlungsdauer erheblich zu. Nach durchschnittlich 5 Jahren kommt es zu verstärkten
Nebenwirkungen, die vor allem mit dem natürlichen Voranschreiten der Erkrankung
zusammenhängen.

### Therapieansatz Dopaminrezeptor-Agonisten

Im Folgenden gehen wir auf die Dopaminrezeptor-Agonisten, als Therapieform ein.

Die erste Generation von Dopaminrezeptor-Agonisten waren Ergotamin-Derivate:

- Bromocriptin (Pravidel®)
- Cabergolin (Cabaseril®)
- α-Dihydroergocriptin (Almirid®)
- Lisurid (Dopergin®)
- Pergolid (Pergolid Hexal®).

In der Therapie des Morbus Parkinson spielen die Ergotamin-Derivate aufgrund ihres
unspezifischen Rezeptorprofils und der Fibrosierung von Herzklappen mit konsekutiver
Klappeninsuffizienz keine Rolle mehr.

D2-Agonisten

- Pramipexol (Sifrol®)
- Ropinirol (Requip®)
- Piribedil (Clarium®)
- Apomorphin (APO-go®)
- Rotigotin (Neupro®).

### Wirkung Dopaminrezeptor-Agonisten

Kommen wir zur Wirkung der Dopaminrezeptor-Agonisten. Die dopaminergen Agonisten
aktivieren die hemmenden D2-Rezeptoren postsynaptischer Neurone. Ihre Wirkung ist vom
Degenerationszustand der präsynaptischen Neurone unabhängig. Sie wirken deswegen auch
noch im fortgeschrittenen Krankheitsstadium.

**Pramipexol** und **Ropinirol** waren die ersten Medikamente der Nicht-ErgotaminAntiparkinsonmittel. Ropinirol kann als Pflaster verwendet werden und hat eine höhere
Affinität zum D3- als zum D2-Rezeptor. Diese Wirkstoffe weisen eine D2-Rezeptor-Selektivität
auf, wodurch bei diesen Präparaten ein niedrigeres Risiko einer Störung des Kreislaufsystems
besteht.

**Piribedil** ist ein D2-Agonist welcher zusätzlich zentrale α2-Adrenozeptoren hemmt und
weniger Müdigkeit als Pramipexol bzw. Ropinirol verursacht. Allerdings bewirkt dieser
Wirkstoff eine Steigerung des Blutdruckes, dies ist bei Patienten mit zu niedrigem Blutdruck
ein Vorteil. Ebenso geht dies mit einer geringeren Chance zur Ödembildung ein.

**Apomorphin**, ein Derivat des Morphins, ist ein unselektiver D1- und D2-Rezeptor-Agonist
ohne opioide Wirkung. Aufgrund seiner hohen Polarität kann es nur per Injektion oder
Infusion appliziert werden und wirkt stark Brechreiz auslösend sowie Blutdruck senkend.

**Rotigotin** ist ein selektiver D2- und D3-Rezeptor-Agonist, welcher als Pflaster appliziert wird.
Durch die dauerhafte Aufnahme des Wirkstoffes über die Zeit setzt sich im Körper ein stabiler
Dopaminspiegel ein, welcher bei den Patienten den Schlaf und die morgendlichen
motorischen Symptome verbessert. Unerwünschte Wirkungen wie Übelkeit, Erbrechen oder
Benommenheit treten dennoch auf.

Zur Vermeidung schwerer dopaminerger Nebenwirkungen müssen DopaminrezeptorAgonisten langsam aufdosiert werden, sodass die volle Wirkung erst nach 1 bis 2 Wochen
erreicht wird.

Bis auf Apomorphin und Rotigotin werden alle Dopamin-Agonisten in oraler Form
aufgenommen. Bislang wurden Dopamin-Agonisten v.a. in der Frühphase der Erkrankung
eingesetzt, wobei aktuell jedoch eher eine frühzeitige L-Dopa-Therapie angestrebt wird.
Dopamin-Agonisten wird in der Behandlung als einziges Medikament verwendeten, wenn der
Patient eine L-Dopa Unverträglichkeit aufweist. Bei einer Kombinationstherapie aus
mehreren Medikamenten vermindern Dopamin-Agonisten die durch L-Dopa ausgelösten
motorischen Störungen.

Kommen wir nun zu den unerwünschten Wirkungen, welche bei einer Behandlung mit
Dopaminrezeptor-Agonisten auftreten können.

Durch die Stimulation der D2-Rezeptoren werden charakteristische Nebenwirkungen
verursacht. Hierzu gehören Kreislaufbeschwerden bei schnellen Bewegungen mit
Ödembildung durch eine Gefäßerweiterung, Schwindel, Übelkeit und Erbrechen.

Unerwünschte psychische Wirkungen sind Halluzinationen, Desorientiertheit,
Verwirrtheitszustände, Verlust der Impulskontrolle, Steigerung der Libido, aber auch
Tagesmüdigkeit bis hin zu Schlafattacken, besonders unter Pramipexol und Ropinirol.
Störungen des Bewegungsablaufes treten seltener und später als bei L-Dopa auf.

### Therapieansatz NMDA-Rezeptor-Antagonisten

Wenden wir und nun den NMDA-Rezeptor-Antagonisten zu.
Aktuell im Handel gibt es Amantadin (Amantadin AL®, PK Merz®) welches ein Glutamat-
(NMDA-)Rezeptor-Antagonist ist. Eine Zweitliniensubstanz und nur zur Kombinationstherapie
zugelassen ist Budipin (Parkinsan®), dass auch die MAO-B inhibiert.

Glutamat ist Gegenspieler des Dopamins im Striatum, die beeinflusst vor allem Akinese und
Rigor. Amantadin hemmt zusätzlich den Dopamintransporter und muskarinerge
Acetylcholinrezeptoren. Anders als bei L-Dopa tritt bereits nach einigen Monaten
Therapiedauer ein Wirkungsverlust ein. Amantadin ist zusätzlich zu seiner kurzen
Wirkungsdauer auch schwächer wirksam als L-Dopa.

Da Amantadin sich allerdings einen schnellen Wirkungseintritt aufweist, kann es als „Start“ in
die Therapierung verwendet werden. Die Einnahme erfolgt auf dem Oralen weg, nur bei
einer plötzlichen und akuten Verschlechterung der der motorischen Symptomatik wird der
Stoff injiziert.

### Wirkungen NMDA-Rezeptor-Antagonisten

Kommen wir abschließend wieder zu den unerwünschten Wirkungen.

Im Vergleich zu L-Dopa fallen die unerwünschten Wirkungen gering aus. Selten treten
Knöchelödeme mit marmorierter Haut, eine Vergrößerte Prostata, Kreislaufprobleme mit
Schwindel, Mundtrockenheit, Übelkeit, Unruhe, Verwirrtheit, Albträume, Halluzinationen bis
hin zu psychotischen Zuständen auf.

### Therapieansatz MAO-B-Hemmer

Als nächstes behandeln wir die Selektiven MAO-B-Hemmer.

Die Wirkstoffe und Handelsnahmen dieser Medikamente sind:

- Selegilin (Selegilin Stada®)
- Rasagilin (Azilect®)
- Safinamid (Xadago®).

MAO-B-Hemmern wirken in dem sie eine selektive Hemmung der Monoaminooxidase B
(MAO-B) auswirken. Dies verringert den Abbau von Dopamin und verlängert so die
Dopaminwirkung. Ebenfalls wird dadurch der Bedarf an L-Dopa verringert. Zusätzlich wird
durch den verringerten Abbau von Dopamin weniger Wasserstoffperoxid freigesetzt, ein
Radikal, welches als potenziell neurotoxisch eingestuft wird.

MAO-B-Antagonisten können initial als gut verträgliche Medikamente eingesetzt werden.
Diese werden besonders bei leichter Parkinson-Symptomatik oder als Zusatzmedikation zu LDopa verabreicht.

### Wirkungen MAO-B-Hemmer.

Abschließend noch einmal zu den unerwünschten Wirkungen der MAO-B-Hemmer.

Speziell Selegilin kann zu Mundtrockenheit, Schlaflosigkeit, Appetitlosigkeit, Übelkeit,
Kopfschmerzen, Herzrhythmusstörungen, Dyskinesien, Unruhe und psychotischen
Symptomen wie Halluzinationen führen. Für Rasagilin sind schmerzende Gelenke,
grippeähnliche Symptome und depressive Verstimmungen als unerwünschte Wirkungen
bekannt.

Bei einer zu hohen Dosierung kann zusätzlich zur MAO-B auch die MAO-A gehemmt werden,
was zu Nebenwirkungen wie Herzrasen und Bluthochdruck führt.

### Therapieansatz COMT-Hemmer

Als nächstes behandeln wir die COMT-Hemmer. Bei diesen Medikamenten und Wirkstoffen handelt
es sich um.

- Entacapon (Entacapon-neuraxopharm®)
- Tolcapon (Tasmar®).

Das Wirkprinzip hinter COMT-Hemmern basiert auf der reversiblen Hemmung der CatecholO-Methyl-Transferase (COMT). Für die Verstoffwechslung von Dopamin zu zu 3-O-MethylDopa ist diese Transferase zuständig. Durch die Hemmung dieser findet ein verminderter
Abbau von L-Dopa und Dopamin und somit eine verstärkte Dopaminwirkung statt. Daraus
resultieren ein verminderter L-Dopa-Bedarf.

Tolcapon hemmt sowohl die periphere als auch zentrale COMT, während Entacapon nur eine
Hemmung der peripheren COMT bewirkt.

Diese Medikamente werden besonders in fortgeschrittenen Stadien des Morbus Parkinson
und inbesondere bei Fluktuationen in der Beweglichkeit verabreicht. Die Gabe erfolgt
ausschließlich in Kombination mit L-Dopa. Entacapon wird oftmals in der Kombination mit LDopa indiziert, wenn L-Dopa keine ausreichende Wirkungszeit hat, sowie bei End-of-DoseFluktuationen. Tolcapon wird nur dann eingesetzt wenn eine unzureichender Wirkung oder
Unverträglichkeit anderer Anti-Parkinson-Mittel diagnostiziert ist.

### Wirkungen COMT-Hemmer

Nebenwirkungen lassen sich nicht von den Nebenwirkungen der zusätzlich verabreichten
dopaminergen Wirkstoffe differenzieren. Tolcapon ist giftig für die Funktionszellen der Leber,
weshalb es zeitweilig vom Markt genommen wurde.

### Therapieansatz Anticholinergika

Abschließend kommen wir auf die letzte mögliche Therapieform, Anticholinergika, zu sprechen. Diese
werden in der heutigen Medizin allerdings nicht mehr eingesetzt.

Die M-Cholinrezeptor-Antagonisten Biperiden (Akineton®), Metixen (Tremarit®) und
Trihexyphenidyl (Artane®, Parkopan®) sind die ältesten Parkinson-Medikamente und wurden
früher bei Morbus Parkinson eingesetzt. Sie werden aber aufgrund der deutlichen,
klassischen anticholinergen Nebenwirkungen nicht mehr empfohlen.

### Wirkung Anticholinergika

Wirkung

Eine Hauptnebenwirkung der Behandlung ist die Verschlechterung der kognitiven Defizite bei
älteren Patienten. Weitere typischen Nebenwirkungen sind Obstipation, Harnverhalt,
Akkommodationsstörung sowie eine Erhöhung des Augeninnendrucks.

### Quellen

- https://viamedici.thieme.de/lernmodul/8656993/subject/neurologie/erkrankungen+des+gehirns+und+seiner+h%C3%BCllen/basalganglienerkrankungen/hypokinetische+erkrankungen/idiopathisches+parkinsonsyndrom+morbus+parkinson#_B7AB7F3A_73AA_43D2_A675_48D02CAB379B
- https://viamedici.thieme.de/lernmodul/8659023/subject/pharmakologie/zentrales+nervensystem/antidepressiva/anti-parkinson-mittel
- https://viamedici.thieme.de/api/images/original/u/n/k/t/e/pharma_008100_angriffspunkte.jpg?16294954751900000 Bild angriffpunkt Anti-Parkinson-Mittel
- https://viamedici.thieme.de/api/images/original/-/d/o/p/a/pharma_008100_l-dopa.jpg?16294954751900000 wirkung L-Dopa
