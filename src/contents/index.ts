import {inject, Plugin} from "vue";

export interface Chapter {
    id: string;
    title: string;
    text: string;
    next?: Chapter
}

function loadChapters(): Chapter[] {
    const chapters: Chapter[] = [];

    const modules = require.context('./', true, /\.md$/);
    const keys = modules.keys();
    keys.sort();

    keys.forEach(key => {
        const parts = key.replace(/^\.\//, "").split("/");
        const fileName = parts.pop()?.replace(".md", "");

        const module = modules(key);
        const html = module.html;
        const attributes = module.attributes;

        const chapter: Chapter = Object.assign(
            {id: fileName},
            attributes,
            {text: html}
        );

        chapters.push(chapter);
    });

    let next: Chapter | undefined = undefined;
    for (let i = chapters.length - 1; i >= 0; i--) {
        const chapter = chapters[i];
        chapter.next = next;
        next = chapter;
    }

    return chapters;
}

export function useChapters(): Chapter[] {
    return inject('chapters') || [];
}

export function createChapters(): Plugin {
    return {
        install(app) {
            const chapters = loadChapters();
            app.provide('chapters', chapters);
        }
    };
}
