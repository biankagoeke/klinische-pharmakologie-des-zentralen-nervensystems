---
id: schmerzen
title: Schmerzen
---

### Thematik Schmerzen

Anhand dieses Beispiels wollen wir nun näher auf die Wirkung von Pharmazeutischen Produkten auf
das Nervensystem eingehen. Jeder kennt es und hat es schonmal erlebt, bei starken Schmerzen
greifen Menschen zu Medikamenten, die Schmerzen lindern und Abhilfe schaffen. Bei leichten
Schmerzen kommen Medikamente wie: Paracetamol, Acetylsalicylsäure und Ibuprofen zum Einsatz.

Diese werden bei leichten bis mittelstarken Schmerzen eingesetzt. Acetylsalicylsäure und
Ibuprofen wirken, in dem sie die Cyclooxygenasen (COX) hemmen und somit die Bildung von
Prostaglandinen vermindern. Sie sind im Körper für Schmerz, Blutgerinnung, Entzündung und
andere physiologische und pathophysiologische Vorgänge verantwortlich. Diese Wirkstoffe
reichern sich bevorzugt im akut entzündeten bzw. verletzen Gewebe, in der Schleimhaut des
Magen-Darm-Trakts und in der Nierenrinde an und gehören zur Gruppe der „sauren
nichtopioiden Analgetika“.

Paracetamol im Gegensatz wirkt in relativ hoher Konzentration im zentralen Nervensystem.
Dort hemmt der Wirkstoff eine bestimmte Unterform eines Enzyms (COX-3), welches für die
Produktion von Prostaglandinen sorgt. Paracetamol gehört zu der Gruppe der „nichtsauren
nichtopioid-Analgetika“. Nichtsaure nichtopioid-Analgetika wie Paracetamol haben nur eine
schlechte entzündungshemmende Wirkung, dafür aber einen guten fiebersenkenden Effekt.
Die genaue Wirkungsweise von Paracetamol ist bis heute nicht bekannt.

Paracetamol beeinflusst außerdem das Endocannabinoid-System, welches
beruhigende und schmerzstillende Effekte vermittelt, sowie das Serotonin-System.
Serotonin ist ein wichtiger Botenstoff des Nervensystems und somit ein
Neurotransmitter. Dieser erfüllt vielfältige Funktionen Unter anderem wirkt er auf die
Blutgerinnung ein.

Somit werden freierhältliche Schmerzmittel unterteilt in:

**Antipyretische Analgetika**: 

nichtsaures Nicht-Opioid-Analgetikum: Paracetamol, Metamizol
schmerzlindernd und fiebersenkend

saure Nicht-Opioid-Analgetika (NSAP): Ibuprofen, Naproxen, Diclofenac, Indometacin
etc.

schmerzlindernd, fiebersenkend, entzündungshemmend
Magenschleimhaut-Schädigung, Verminderung der Nierendurchblutung


### Starke Schmerzen

Bei starken Schmerzen werden allerdings deutlich Stärkere Schmerzmittel eingesetzt. Diese gehören
zu der Klasse der Opioide. Opioide gehören zu den ältesten Schmerzmitteln, welche schon von vor
tausenden von Jahren zur Schmerzlinderung und Betäubung eingesetzt wurden. Als eher schwache
Opioide gelten beispielsweise Codein und Tramadol, mittelstarke bis starke Wirkung haben Morphin,
Pentazocin oder Fentanyl. Opioide gehören generell zu den Verschreibungspflichten Medikamenten
und fallen unter die Gesetzliche Betäubungsmittel-Verschreibungsverordnung – BtMVV. Da bei
Einnahme von bestimmten Opioiden in einer höheren Dosis Rauschzustände ausgelöst werden
können, wird Ihre Ausgabe strengstens überwacht.

Zur Klasse der Opioide zählen sämtliche natürlichen und synthetischen Substanzen, welche
an die Opioidrezeptoren binden und morphinähnliche Eigenschaften zeigen.
Opioidrezeptoren befinden sich im Menschlichen Körper im Rückenmark, unterschiedlichen
Hirngebieten und in den Nervengeflechten des Darms und der Blase. Diese werden unterteilt
in 3 unterschiedliche Typen. Diese sind δ-, κ- und μ-Opioidrezeptoren. Ihre Wirkung basiert
auf der Inhibierung von G-Proteinen, allerdings unterscheiden sich die Rezeptoren in ihrer
Affinität für verschiedene Liganden und ihre Wirkung.

Bei G-Proteinen oder GTPasen handelt es sich um eine inhomogene Gruppe von
Proteinen, welche in der Lage sind, die Guanin-Nucleotide GDP und GTP zu binden.
Bei Liganden handelt es sich um Stoffe, welche üblicherweise reversibel an einen
Rezeptor binden können.

Die Liganden der Opioide sind in 2 Gruppen unterteilbar, bei diesen handelt es sich um
endogene Opioide und exogene Opioide.

Bei endogenen Opioiden handelt es sich um natürliche Liganden der
Opioidrezeptoren. Diese natürlichen und körpereigenen Peptide sind Enkephaline,
Endorphine und Dynorphine, diese werden aus den Vorstufen Proenkephalin,
Proopiomelanocortin bzw. Prodynorphin gebildet. Wenn ein Ligand sich an einen
passenden Opioidrezeptor bindet, wird dadurch die Erregbarkeit der Zelle reduziert.

Bei exogenen Opioiden handelt es sich um halb- oder vollsynthetische Verbindungen,
teilweise auch um Derivate des Morphins. Ebenso gehören die Opiate zu dieser
Gruppe. Bei diesen handelt es sich um die natürlichen Alkaloide aus dem Milchsaft
des Schlafmohns, der als Opium bezeichnet wird und der neben dem
Hauptbestandteil Morphin u.a. auch Codein enthält. Exogenen Opioide
unterscheiden sich in ihrer Affinität zu den verschiedenen Rezeptortypen und ihrer
Kinetik.

Aus Pharmakologischer Sicht werden Opioide nochmals in Opioidrezeptor-Agonisten und
Opioidrezeptor-Antagonisten unterteilt.

Opioidrezeptor-Agonisten vermitteln in Abhängigkeit von den stimulierten
Opioidrezeptoren eine je nach Wirkstoff mehr oder weniger analgetische, sedierende
und suchtauslösende Wirkung. Unerwünschte Nebenwirkungen einer Stimulation
von Opioidrezeptoren sind z.B. Atemdepression, Obstipation, Toleranzentwicklung
und Abhängigkeitspotenzial.

Opioidrezeptor-Antagonisten hemmen dagegen die Wirkung der Opioide an allen
Opioidrezeptortypen und sind somit keine Opioide im eigentlichen Sinn.

### Wirkung von Opioiden allgemein

Wir wenden uns nun der Wirkung von Opioiden im menschlichen Nervensystem zu. Diese
erklären wir kurz am Beispiel von Endorphin. Bei Endorphin handelt es sich um ein
endogenes Morphin, welches vom Körper selbst produziert wird.

Die endogenen Opioide Endorphin und Enkephalin entstehen aus ihre Vorstufen
Proopiomelanocortin bzw. Proenkephalin. Sie stimulieren Opioidrezeptoren. Dort
hemmen sie u.a. den Ca2+-Einstrom, wodurch die Transmitterfreisetzung reduziert
wird, oder steigern die K+-Permeabilität, sodass die zelluläre Erregbarkeit abnimmt.
Naloxon wirkt antagonistisch an den Opioidrezeptoren und hemmt die
Opioidwirkung.

![](pharma_006100_steckbrief.jpg)

Man unterscheidet 3 Typen von Opioidrezeptoren (OR), die Opioide mit unterschiedlicher
Affinität binden:

- μ-Opioidrezeptor (MOR)
- κ-Opioidrezeptor (KOR)
- δ-Opioidrezeptor (DOR).

Diese oben genannten Rezeptoren entwickeln ihre WIrkung über inhibitorische GProteine.
Ihre Aktivierung führt zu einer Hemmung der Adenylatzyklase, was zu einer Verringerung 
von Cyclischem Adenosinmonophosphat cAMP, und einer Schließung von präsynaptischen 
spannungsabhängigen Ca2+-Kanälen führt. Dies hat eine verminderte Transmitterfreisetzung 
zur Folge, welche die Membrandepolarisation erschwert und die Übertragung der Erregung 
an der Synapse beeinträchtigt. 

Lokalisiert sind diese Rezeptorarten im Zentralen Nervensystem und in der Peripherie:

Die Opioidrezeptoren im ZNS befinden sich in spinalen und supraspinalen Synapsen
des aufsteigenden nozizeptiven und des absteigenden antinozizeptiven Systems. Am
Hinterhorn des Rückenmarks vermitteln sie eine Hemmung der
Transmitterfreisetzung und eine Verringerung der neuronalen Erregbarkeit.

Bei einem nozizeptiven Systems handelt es sich um System von Synapsen welcher
unter der „Wahrnehmung“ Schmerz leiden. Somit sind die Synapsen gemeint im
Menschlichen Körper, welche zum aktuellen Zeitpunkt die Wahrnehmung „Schmerz“
haben und ans Gehirn weiterleiten. Bei antinozizeptiven System handelt es sich um
Synapsen, welche bereits unter dem Einfluss von Opioiden stehen und somit schon
einer geringeren Schmerzwahrnehmung unterliegen.

In der Peripherie sind Rezeptoren an den Nozizeptoren lokalisiert und vermitteln eine
Verringerung der Entladungsfrequenz, sowie auch eine Hemmung der
Schmerzweiterleitung. Auf den postganglionären Neuronen des autonomen
Nervensystems führt die Aktivierung der Rezeptoren zu einer Hemmung der
Transmitterfreisetzung.

Dargestellt ist die erste Synapse des aufsteigenden nozizeptiven Systems, die unter
dem Einfluss eines hemmenden Interneurons des absteigenden antinozizeptiven
Systems steht. Hemmende Interneurone setzen als Transmitter u.a. Enkephaline
(Opioidpeptide) frei. Diese endogenen Opioide hemmen, wie therapeutisch
applizierte Opioidanalgetika auch, die synaptische Übertragung zwischen der
nozizeptiven Primärafferenz und dem postsynaptischen Neuron. Sie wirken über 
GiProtein-gekoppelte prä- und postsynaptische Opioidrezeptoren (OR) hemmend auf
spannungsabhängige Ca2+-Kanäle (präsynaptisch) und aktivierend auf
einwärtsgleichrichtende K+-Kanäle (postsynaptisch) und verringern die Erregbarkeit
des Neurons. Diese verringerte Erregbarkeit hat eine verringerte
Schmerzwahrnehmung zur Folge. 

![](pharma_006100_opi_rezeptor_hinterhorn.jpg)

### Einteilung Opioide

Nun da wir die Wirkungsweise von Opioiden kennengelernt haben, gehen wir genauer auf
unterschiedliche Endogene Opioide, exogene Opioide, sowie ihre bevorzugten Opioidrezeptoren ein. 

| Vorstufe            | Endogenes Opioid | Affinität zu den Rezeptortypen |
|---------------------|------------------|--------------------------------|
| Proenkephalin       | Met-Enkephalin   | δ > μ                          |
|                     | Leu-Enkephalin   | δ > μ                          |
| Prodynorphin        | Dynorphin A      | κ > μ                          |
|                     | Dynorphin B      | κ > μ = δ                      |
|                     | α-Neodynorphin   | κ > μ = δ                      |
| Proopiomelanocortin | β-Endorphin      | μ = δ                          |

*Nach Graefe, Lutz, Bönisch, Duale Reihe Pharmakologie und Toxikologie, Thieme, 2016*

Endogene Opioide werden in den Neuronen des zentralen, peripheren und vegetativen
Nervensystems produziert.

Exogene Opioide werden als Therapeutika oder missbräuchlich auch als Rauschmittel angewandt.
Diese weisen eine vielfältige Wirkung auf, wobei wir auf 3 unterschiedliche Wirkungen eingehen
wollen.

Exergone Opioide die als reine Opioidrezeptor-Agonisten kategorisiert werden,
haben vorrangig eine Schmerzlindernde Wirkung auf den Körper.

Loperamid ein Opioidrezeptor-Agonist, der die Acetylcholinfreisetzung hemmt und
daher eine obstipierende Wirkung aufweist. Zudem verzögert Loperamid die
Magenentleerung und verlangsamt die intestinale Passage. Dies bedeutet, dass
dieser Stoff, bei der Einnahme bei Patienten Vorstopfung auslöst.

Methadon ist ein Opioid, welches ebenso in der Schmerztherapie verwendet wird.
Bekannt ist es allerdings vorrangig als Entzugs- und Substitutionsmittel in der
Therapie von Heroinabhängigen.

Opioidanalgetika weisen eine unterschiedliche Wirkung auf, je nachdem an welchen Rezeptortypen
sie sich binden. In der nachfolgenden Tabelle gehen wir auf die unterschiedlichen Wirkungsweisen je
nach Rezeptortyp ein.

| δ-Rezeptoren | κ-Rezeptoren | μ-Rezeptoren |
|--------------|--------------|--------------|
| Anxiolyse (= pharmakologische Wirkungen von Substanzen die Ängste mindern oder gar unterdrücken.) | Dysphorie (=Störung des emotionalen Erlebens, die durch eine ängstlichbedrückte, traurig-gereizte Stimmungslage charakterisiert ist) | Anxiolyse |
| Sedierung (=Dämpfung von Funktionen des zentralen Nervensystems durch ein Beruhigungsmittel) | Sedierung | Sedierung |
| Euphorie | Euphorie | Euphorie |
| Obstipation (=Verstopfung) | Atemdepression (=Abflachung bzw. Herabsetzung der Atmung, durch eine Beeinträchtigung der Atemsteuerung durch das ZNS oder PNS) | Atemdepression |
| Analgesie (untergeordnet)<br>(=Schmerzlinderung) | Analgesie (untergeordnet) | Analgesie (hauptsächlich) |
| | Miosis (=temporäre Verengung der Pupille durch Kontraktion des Musculus sphincter pupillae oder verminderte Aktivität des Musculus dilatator pupillae) | Miosis |
| | Diurese (=Harnausscheidung durch die Nieren. „Man muss aufs Klo“) | Antidiurese (= „Man muss weniger aufs Klo“) |
| | | Prolaktinspiegel im Plasma steigt (Hormon des Körpers welches zusammen mit anderen Hormonen für die Milchproduktion in der Brustdrüse sorgt. Ein erhöhter Prolaktinspiegel kann allerdings zu Unfruchtbarkeit führen |
| | | Obstipation (=Verstopfung) |
| | | Übelkeit |
| | | Störungen der Sexualhormone |
| | | Abhängigkeit |

*Nach Graefe, Lutz, Bönisch, Duale Reihe Pharmakologie und Toxikologie, Thieme, 2016*

Zur Verdeutlichung die wichtigsten Wirkungen der unterschiedlichen Rezeptoren in einem
Bild. Insbesondere die Aktivierung der μ-Rezeptoren vermittelt eine stark analgetische
Wirkung, damit einher geht jedoch auch ein hohes Abhängigkeitspotenzial.

![](pharma_006100_wirkung_opioidrezeptoren.jpg)

*Abbildung 2 aus Lüllmann, Mohr, Wehling et al., Pharmakologie und Toxikologie, Thieme, 2016*

### Liganden der Opioidrezeptoren

Wir widmen uns nun genauer den verschiedenen Liganden der Opioidrezeptoren. Wie bereits gelernt
unterteilen sich die Liganden aus der Pharmakologischen Sicht nochmals in Agonisten und
Antagonisten. Diese Liganden werden je nach Affinität zu den jeweiligen Rezeptoren und der
intrinsischen Aktivität nochmals unterschieden in:

**Reine Agonisten:** Diese binden mit hoher Affinität an den μ-Rezeptor, aber nur eine geringe Aktivität
am κ-Rezeptor. Zu ihnen zählen die endogenen Opioide wie auch die stark wirksamen
Opioidanalgetika.

**Partielle Agonisten:** Diese Liganden binden mit einer höheren Affinität als Morphin an den μ-Rezeptor,
besitzen aber eine geringere intrinsische Aktivität als Morphin. Zu ihnen zählt z.B.
Buprenorphin.

**Gemischte Agonisten-Antagonisten:** Je nach Rezeptortyp wirken diese Stoffe agonistisch,
partiell agonistisch oder auch antagonistisch. 
Ein Vertreter dieser Gruppe ist Nalbuphin (μ-antagonistisch und κagonistisch).

**Reine Antagonisten:** Bei reinen Antagonisten handelt es sich um kompetitive Antagonisten an den
Opioidrezeptoren. Vertreter dieser Gruppe sind Naloxon, Naltrexon und
Methylnaltrexon. Sie binden und hemmen die Wirkung der Opioide an allen
Opioidrezeptortypen. Aus diesem Grund gehören im eigentlichen Sinne nicht der
Gruppe der Opiode an. Sie werden zur Therapie von Opioidintoxikationen und -
überdosierungen verabreicht.

### Wirkungen von Opioiden

Da wir nun die Wirkungsweise der Opioide sowie ihre Funktion verstanden haben gehen wir nun
genauer darauf ein, welche Erwünschte und unerwünschte Wirkungen diese im Menschlichen Körper
auslösen.

Zu den erwünschten Effekten gehört besonders die analgetische Wirkung, über die eine
Stimulation der Opioidrezeptoren ausgelöst wird, den wohl wichtigsten Molekülen des
antinozizeptiven Systems. Zusätzlich vermitteln sie eine antidiarrhoische Wirkung und
hemmen das Hustenzentrum leicht. Ebenso sind die Sedierung, Enspannung und
Verminderung der Schmerzangst ein gewünschter Effekt der Opioide.

Zu den unerwünschten Effekten gehört die durch Stimulation der Chemorezeptoren in der
Area postrema aufrufbare Übelkeit und Erbrechen. Über die Erregung parasympathischer
Kerngebiete lösen sie eine Miosis aus. Durch die Steigerung des Tonus der glatten Muskulatur
im Gastrointestinaltrakt wird die Propulsivmotorik gehemmt, sodass eine schwerwiegende
Obstipation resultieren kann. Außerdem ist die Entleerung der Harnblase behindert.
Dosisabhängig kommt es auch zu einer Hemmung des Atemzentrums. Bei wiederholter
Zufuhr von Opioiden, wie z.B. während einer Tumortherapie, kann eine Toleranzentwicklung
hervorgerufen werden, sodass immer höhere Dosierungen nötig sind, um die gleiche
schmerzlindernde Wirkung zu erzielen. Die euphorisierende Wirkung der Opioide
(Opioidrausch) ist verantwortlich für die Entstehung einer Abhängigkeit. Wegen des hohen
Abhängigkeits- und Missbrauchspotenzials werden die meisten Opioide nach dem
Betäubungsmittelgesetz (BtMG) mit BtM-Rezepten verschrieben. Ebenso wie ein
gewünschter Effekt kann die Sedierung aber auch als unerwünschter Effekt auftreten.

![](pharma_006200_steckbrief.jpg)

### Quellen

- https://www.apotheken-umschau.de/medikamente/basiswissen/schmerzmittel-welches-hilft-wann717725.html
- https://www.netdoktor.de/medikamente/paracetamol/
- https://de.wikipedia.org/wiki/Cyclooxygenasen
- https://de.wikipedia.org/wiki/Prostaglandine
- https://de.wikipedia.org/wiki/Nichtsteroidales_Antirheumatikum#cite_note-2
- https://www.scinexx.de/dossierartikel/morphin-codein-co/
- https://m.thieme.de/viamedici/klinik-faecher-anaesthesie-1529/a/schmerztherapie-4271.htm
- https://viamedici.thieme.de/lernmodul/5617705/subject/pharmakologie/analgetika/opioide/opioide+%C3%BCberblick
- https://flexikon.doccheck.com/de/G-Protein
- https://viamedici.thieme.de/api/images/original/b/r/i/e/f/pharma_006100_steckbrief.jpg?16260857739130000 Bild Wirkung Von Opioiden
- https://viamedici.thieme.de/api/images/original/r/h/o/r/n/pharma_006100_opi_rezeptor_hinterhorn.jpg?16260857739130000 Synapse Opioide
- https://viamedici.thieme.de/api/images/original/t/o/r/e/n/pharma_006100_wirkung_opioidrezeptoren.jpg?16260857739130000 Kurzübersicht Wirkung Opioide an Rezeptoren
- https://viamedici.thieme.de/api/images/l/b/r/i/e/f/pharma_006200_steckbrief.jpg?16272886226130000 Bildliche darstellung von Effekten und Nebenwirkungen
