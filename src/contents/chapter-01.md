---
id: ueberblick
title: Überblick
---

### Pharmakologie des Zentralen Nervensystems

Das Nervensystem steht im Mittelpunkt bei vielen Pharmazeutischen Produkten. Um die
Wirkungsweise und die klinische Anwendung dieser Substanzen zu verstehen, sind bestimmte
Grundkenntnisse über den allgemeinen Aufbau und die Physiologie des Nervensystems erforderlich.
Aus diesem Grund werden wir zunächst ein Grundverständniss für das Nervensystem aufbauen.
Die Grundbausteine des Nervensystems sind die Nervenzellen mit ihren Nervenfasern sowie das
Stütz- und Ernährungsgewebe. Zum besseren Verständnis wird das Nervensystem in 2 Komponenten
unterteilt, die jedoch anatomisch und funktionell untrennbar miteinander verbunden sind:

![](Nervensystem.png)

- zentrales Nervensystem (ZNS) und
- peripheres Nervensystem.

**Das zentrale Nervensystem** umfasst die innerhalb des Schädels und der Wirbelsäule
eingeschlossenen Anteile, also das Gehirn und das Rückenmark. Gehirn und Rückenmark werden von
den afferenten (= hinführenden) Nerven mit Sinneseindrücken aus der Außenwelt oder Signalen von
den inneren Organen versorgt. Im Normalfall werden diese vom Rückenmark zum Gehirn
weitergeleitet, das als eine Art oberster Befehlshaber entscheidet, was nun zu tun ist.

**Das periphere Nervensystem** besteht aus Nervenzellen und Nervenfaserbündeln, die das zentrale
Nervensystem mit den Sinnesorganen (wie Auge, Ohr usw.) und den Erfolgsorganen (wie Muskeln,
Sinnesrezeptoren usw.) verbinden. Diese peripheren Anteile sind die Gehirnnerven und die
Spinalnerven. Die Spinalnerven sind mit dem Rückenmark über eine vordere und hintere Wurzel
verbunden. Die Nerven sind Faserbündel, wobei jede Faser mit dem Körper einer einzelnen
Nervenzelle verbunden ist. Das periphere Nervensystem lässt sich noch weiter in folgende
funktionelle Einheiten unterteilen:

- somatisches Nervensystem 
- vegetatives Nervensystem

**Das somatische Nervensystem**, ist Teil des peripheren Nervensystems, können wir bewusst
steuern. Er hilft uns immer dort, wo Bewegung notwendig ist, also zum Beispiel beim Laufen oder bei
komplexen Bewegungsabläufen wie sie unserem Körper beim Sport abverlangt werden (zum Beispiel
der Aufschlag beim Tennis oder auch Yoga-Übungen). Dabei kommt es immer zu einem
Zusammenspiel von sensorischen und motorischen Nerven. Die sensorischen Nerven sind mit
unseren Sinnesorganen verbunden, zum Beispiel unseren Augen, die beim Aufschlag den Tennisball
fixieren.

Die motorischen Nerven hingegen gehen zu den Muskeln und sorgen dafür, dass diese sich
zusammenziehen (= anspannen) oder entspannen und wir die Aufschlagbewegung durchführen und
den Ball beschleunigen können.

**Das vegetative Nervensystem**, das auch als unwillkürliches Nervensystem bezeichnet wird, kann
selbst ebenfalls wieder untergliedert werden. Seine drei Teilbereiche sind der Sympathikus und sein
Gegenspieler der Parasympathikus, sowie das enterische Nervensystem (ENS, oft auch als
Eingeweide- oder Darmnervensystem bezeichnet oder mit dem englischen Ausdruck „abdominal
brain“ treffend umschrieben).

Die zwei Teile des vegetativen Nervensystems sind die beiden Gegenspieler Sympathikus und
Parasympathikus. Was beide gemeinsam haben, ist die Tatsache, dass wir sie nicht bewusst
steuern können – die Reaktionen laufen unwillkürlich ab. Beide Systeme sind uralte
Schutzmechanismen für unseren Körper, die uns helfen, in Gefahrensituationen schnell zu
handeln. Nehmen wir das Beispiel eines unserer Vorfahren, der auf der Jagd von einem
Säbelzahntiger überrascht wird. Er hat nun zwei Möglichkeiten: Entweder kämpft er gegen
das Tier oder er flieht (Fight-or-Flight). In beiden Fällen muss der Körper rasch mit Energie
versorgt werden. Dies ist die Aufgabe des Sympathikus, der beispielsweise unsere Atmung
beschleunigt, sodass mehr Sauerstoff zu den Muskeln gelangt, und die Energiegewinnung
antreibt. Hat der Ur-Mensch den Säbelzahntiger dann besiegt oder ist er erfolgreich
geflüchtet, kann er sich anschließend ausruhen. Nun wird der Parasympathikus aktiv und
sorgt dafür, dass der Körper sich entspannen kann. Die durch den Sympathikus abgestellte
Verdauung wird wieder aktiviert und Blutdruck und Puls sinken wieder.

Auch in der heutigen Zeit greifen diese Mechanismen: Vor einem wichtigen beruflichen
Termin oder einer Prüfung ist der Sympathikus aktiv, danach der Parasympathikus. Entlädt
sich diese Energie allerdings nicht, kann dies auf Dauer zu negativem Stress führen, der uns
krank macht.

Das enterische Nervensystem (auch „Gehirn des Darms“ genannt,) ist ein komplexes eigenständiges
(intrinsisches) nervales Netzwerk, das sich entlang der gesamten Wand des Magen-Darm-Trakts vom
Ösophagus bis zum Rektum zieht. Es steuert hauptsächlich die Motilität und die Sekretion des
Magen-Darm-Trakts und besteht aus 2 Ganglienzellschichten, dem Plexus myentericus (Auerbach)
und dem Plexus submucosus (Meissner).

Weiterhin wird noch ein autonomes Nervensystem unterschieden, das ebenfalls einen zentralen und
einen peripheren Anteil besitzt. Es besteht aus einer Ansammlung von Nerven und Ganglien
(Ansammlung von Nervenzellen) durch die das Herz, die Blutgefäße, Eingeweide, Drüsen usw. mit
Nerven versorgt werden (Innervation). Diese Organe funktionieren autonom, d. h. unabhängig vom
Willen des Menschen und sind doppelt mit Nerven versorgt: durch das sympathische Nervensystem
und durch das parasympathische Nervensystem.

Gehen wir nun auf die Erregungsübertragung an den Synapsen ein, da wir dies für das spätere
Verständnis noch brauchen und aus diesem Grund nochmal auffrischen sollten. 

Eine Erregungsübertragung von einer Synapse auf eine andere, erfolgt im Endknöpfchen. Der
Auslöser für die Reaktionen der Synapse ist ein Aktionspotenzial, das vom Axon kommt und
die Membran des synaptischen Endknöpfchen depolarisiert. Dieses Aktionspotential ist ein
elektrisches Signal, welches zur Folge hat, dass spannungsgesteuerte Calcium-Ionenkanäle
geöffnet werden und Calciumionen (Ca2+) einströmen. Das Calcium bewirkt, dass Vesikel, die
mit Neurotransmitter (Acetylcholin) gefüllt sind, mit der präsynaptischen Membran
verschmelzen und die Transmitter in den synaptischen Spalt ausschütten. Diese diffundieren
zur postsynaptischen Membran und binden sich an spezifischen Rezeptoren von
Ionenkanälen (z.B. Natriumionenkanäle). Diese Kanäle sind nicht spannungsgesteuert wie die
Kanäle auf der präsynaptischen Membran oder die auf dem Axon, sondern Liganden
gesteuert.

Durch die geöffneten Ionenkanäle strömen nun beispielsweise Natrium-Ionen (Na+) ein und
es kommt zu einer Depolarisation der postsynaptischen Membran. Ein Aktionspotenzial
entsteht und wird weitergeleitet. Die Frequenz und Stärke des Aktionspotenzials hängt von
der Konzentration des Neurotransmitters, im synaptischen Spalt, ab. Durch eine hohe
Frequenz, die bei der Membran des synaptischen Endknöpfchen ankommt, wird auch eine
hohe Transmitterkonzentration im synaptischen Spalt erreicht und es kommt zu einer
entsprechend höheren Frequenz von Aktionspotenzialen auf der postsynaptischen Membran.

Solange Acetylcholin im synaptischen Spalt vorhanden ist, findet die Reizweitergabe statt.
Das Enzym Cholinesterase baut den Neurotransmitter ab, indem es ihn in seine Bestandteile
Acetat und Cholin spaltet, und stoppt so die Weitergabe der Erregung. Acetat und Cholin
werden zur präsynaptischen Membran zurückgeführt, wieder im Endknöpfchen
aufgenommen und durch das Enzym Cholinacetyltransferase zu Acetylcholin verbunden. Es
steht für die nächste Erregungsweiterleitung zur Verfügung.

![](synapse-0-ca.png)

*Abbildung 1: https://www.studienkreis.de/fileadmin/lernen/assets/courses/media/synapse-0-ca.png*
